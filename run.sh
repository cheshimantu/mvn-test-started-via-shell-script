mkdir -p /target/allure-results
export ALLURE_RESULTS=/target/allure-results
echo "We created ${ALLURE_RESULTS}"
./mvnw clean -Denv=dev -DisRemote=true -DremoteAddress=http://selenoid:4444/ test
ls -la ${ALLURE_RESULTS}